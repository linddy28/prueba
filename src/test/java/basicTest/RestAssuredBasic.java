
package basicTest;




import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.File;

import static io.restassured.RestAssured.given;

public class RestAssuredBasic {

    /*
      given()----> configuracion
      when() ----> request action --> URL ---> Method
      then() ----> verificaciones
     */

    @Test
    public void createProject(){
        given()
                .header("Authorization","Basic dWNiQHVjYjIwMjEuY29tOjEyMzQ1")
                .body("{\n" +
                        "  \"Content\":\"UCB_Lindsay\",\n" +
                        "  \"Icon\":\"5\"\n" +
                        "}")
                .log()
                .all()
                .when()
                .post("http://todo.ly/api/projects.json")
                .then()
                .log()
                .all();


    }

    @Test
    public void createProjectExternalFile(){
        given()
                .auth()
                .preemptive()
                .basic("ucb@ucb2021.com","12345")
                .body(new File("C:\\Users\\akiko\\Desktop\\ucb\\src\\test\\resources\\projectBody.json"))
                .log()
                .all()
                .when()
                .post("http://todo.ly/api/projects.json")
                .then()
                .log()
                .all();
    }

    @Test
    public void createProjectJSONObject(){
        JSONObject body= new JSONObject();
        body.put("Content","LindsayJSONObject");
        body.put("Icon",4);

        given()
                .auth()
                .preemptive()
                .basic("lindsay@2021.com","12345")
                .body(body.toString())
                .log()
                .all()
                .when()
                .post("http://todo.ly/api/projects.json")
                .then()
                .log()
                .all();
    }

}
